<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use adman9000\binance\BinanceAPI;
use Illuminate\Support\Facades\Redis;

class NewController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $allEthTradePairs;

    public function __construct(){

        $getTickers = new BinanceAPI;
        $allPairs = $getTickers->getTickers();
        //dd($allPairs);
        foreach($allPairs as $pair){

            foreach($pair as $symbol => $name){

                if($symbol = 'symbol'){
                    preg_match('/^.*ETH$/', $name, $match);
                    if(array_key_exists(0, $match)){
                       $allEthTradePairs[] = $match[0];
                    }
               }


            }
        }

        $this->allEthTradePairs = $allEthTradePairs;
  
    }


    public function redis(){

        Redis::set('name', 'Taylor');

        dd();

    }


    public function candeStick(){

        $tradePair = 'BNBBTC';
        $candleTimeInterval = 1;
    

        $candle = new BinanceAPI;
        return $candle->candleStick($tradePair,$candleTimeInterval);
    }



    /**
     * check api, this is giving back all the pairs, api mistake
     */

    public function getTicker(){

        $getTicker = new BinanceAPI;
        return $getTicker->getTicker('ELFETH');
        

    }





    public function getAllEthTradePairs(){
        

        foreach($this->allEthTradePairs as $key => $pair)
        {
            dump($pair);

        }
          //  dump($this->allEthTradePairs);
        
    

    }

    public function getMarkets(){

        $markets = new BinanceAPI;
        return $markets->getMarkets();
/**
 * [  
   {  
      "symbol":"ETHBTC",
      "status":"TRADING",
      "baseAsset":"ETH",
      "baseAssetPrecision":8,
      "quoteAsset":"BTC",
      "quotePrecision":8,
      "orderTypes":[  
         "LIMIT",
         "LIMIT_MAKER",
         "MARKET",
         "STOP_LOSS_LIMIT",
         "TAKE_PROFIT_LIMIT"
      ],
      "icebergAllowed":true,
      "filters":[  
         {  
            "filterType":"PRICE_FILTER",
            "minPrice":"0.00000100",
            "maxPrice":"100000.00000000",
            "tickSize":"0.00000100"
         },
         {  
            "filterType":"LOT_SIZE",
            "minQty":"0.00100000",
            "maxQty":"100000.00000000",
            "stepSize":"0.00100000"
         },
         {  
            "filterType":"MIN_NOTIONAL",
            "minNotional":"0.00100000"
         }
      ]
   },
 */
    }

    /**
     * $symbol = 'BNBBTC', $limit = 500
     */

    public function getRecentTrades($symbol = 'BNBBTC', $limit = 500){

        $recentTrades = new BinanceAPI;
        return $recentTrades->getRecentTrades('BNBETH', 500);
    /**
     * what did you buy on what price, and when...       
     * 
      "id":5634428,
      "orderId":23041114,
      "price":"0.02845100",
      "qty":"25.50000000",
      "commission":"0.01275000",
      "commissionAsset":"BNB",
      "time":1523438029552,
      "isBuyer":true,
      "isMaker":true,
      "isBestMatch":true
   },
     */
         
    }


}

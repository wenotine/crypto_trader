<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Binance authentication
    |--------------------------------------------------------------------------
    |
    | Authentication key and secret for Binance API.
    |
     */

    'auth' => [
        'key'        => env('BINANCE_KEY', 'gU1evjtD9YFYA9YZjgF8jBWurTw8PzPzqzegUsXvZORQPycFpOTiLgWWSFLFT4Od'),
        'secret'     => env('BINANCE_SECRET', '6iVaTeOfwnw3bDnocHbCF79F759uwLVhqfW9UYh27eBM40HFCV459HYVCJANvhFt')
    ],

    /*
    |--------------------------------------------------------------------------
    | API URLs
    |--------------------------------------------------------------------------
    |
    | Binance API endpoints
    |
     */

    'urls' => [
        'api'  => 'https://www.binance.com/api/',
        'wapi'  => 'https://www.binance.com/wapi/'
    ],


    /*
    |--------------------------------------------------------------------------
    | API Settings
    |--------------------------------------------------------------------------
    |
    | Binance API settings
    |
     */

    'settings' => [
        'timing' => env('BINANCE_TIMING', 5000),
        'ssl'    => env('BINANCE_SSL_VERIFYPEER', true)
    ],

];

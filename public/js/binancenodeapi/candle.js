require('es6-promise/auto')
var candleCode = require('./candle')
var apiSecret = require('./secretkey')
const Binance = require('binance-api-node').default

const client2 = Binance({
    apiKey: 'oiachbIkzzDM8gph5qTHlIqEZCoTPJiHqLuViX9mhn90TY9JajrZkPzouixPdmSs',
    apiSecret: apiSecret.getApiSecret(),
})

var symbols = ["EOSETH", "TRXETH", "BCCETH", "XRPETH", "ICXETH", "BNBETH", "ZILETH", "ADAETH", "XVGETH", "ELFETH",
    "NEOETH", "ONTETH", "BNTETH",
    "IOTAETH", "IOSTETH", "AMBETH", "WANETH", "XLMETH", "LTCETH", "DNTETH", "BTGETH", "VENETH", "CMTETH", "GTOETH",
    "AIONETH", "MANAETH", "KNCETH", "OMGETH",
    "QTUMETH", "NCASHETH", "POAETH", "BCDETH", "PPTETH", "ETCETH", "NANOETH", "WPRETH", "DASHETH", "XMRETH",
    "REQETH", "STORMETH", "WTCETH", "BQXETH", "NEBLETH", "QSPETH", "POWRETH", "POEETH", "ZRXETH", "ASTETH",
    "XEMETH", "AEETH", "CNDETH", "DGDETH",
    "MODETH", "STEEMETH", "ENGETH", "QLCETH", "HSRETH", "BLZETH", "LSKETH", "LRCETH", "BATETH"]


let arraySymbols = []
var sortedWinners = []


module.exports.win = function(){

    symbolsPromiseArray = []
    
    symbols.forEach(function (symbol, index) {
        

            symbolsPromiseArray.push(getCandles(symbol))
        
    })
    return symbolsPromiseArray
}




async function testPing(symbol) {

    var ping = await client2.ping()
    return ping
    
}

    async function getCandles(symbol) {

    
    if( testPing() ){

        
        
        let candles = await client2.candles({
            symbol: symbol,
            limit: 9,
            interval: "5m"
            
        })
        
        let candlesArray = []
        let percentageArray = []
        let finalPercentageArray = []
        var sumNumbers = (accumulator, currentValue) => accumulator + currentValue;
        
        
        candles.forEach(function (element, index) {
            
            
            if (index == 0) {
                percentageArray.push(element.close)
            } else if (index == 6) {
                percentageArray.push(element.close)
            }
            
            
            if (index < 7) {
                
                
                if (candles[index + 1].close > element.close) {
                    candlesArray.push(0)
                } else {
                    candlesArray.push(1)
                }
            }
        })
        var percentage = (percentageArray[1] / percentageArray[0]) * 100
        
        var arraySum = candlesArray.reduce(sumNumbers)
        
        if (arraySum > 5 && percentage < 99) {
            
            // console.log(symbol+": " + candlesArray)
            return [symbol, candlesArray, percentage]
        } else {
            return null
        }
        
    }
}
    
    
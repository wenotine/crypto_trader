require('es6-promise/auto')
var candleCode = require('./candle')
var apiSecret = require('./secretkey')
const Binance = require('binance-api-node').default

//require('es6-promise').polyfill()


const client = Binance()

// Authenticated client, can make signed calls
const client2 = Binance({
    apiKey: 'oiachbIkzzDM8gph5qTHlIqEZCoTPJiHqLuViX9mhn90TY9JajrZkPzouixPdmSs',
    apiSecret: apiSecret.getApiSecret(),
})
var a,b
var main = []
var seconds = []
var sumSecondsArray = []
var sumBlockArray = []
var finalBlocksToStore = []
var dbBlock;
var blockEight =[]

var winner = []

var recvWindowMilisec = 10000

var readyArryay = []
async function getData(){


    for( var value of candleCode.win() ){

        var iHaveIt = await value
        if(iHaveIt){

            readyArryay.push(iHaveIt)
        }
    }

    return readyArryay

}
var tradePair = ''

async function testPing(symbol) {

    var ping = await client.ping()
    console.log(ping)
    return ping
}

function wrapper(){

    console.log("Binance Trader Started...")

if( testPing() ) {



    getData().then(function(data){
        
        
        if( data.length > 0 ){
            
            tradePair = data
            
            
            let maxRand = getRandomInt( 0, (data.length -1) )  
            
            tradePair = data[maxRand][0] 
            sevenReadyBlocks = data[maxRand][1]
            
            if( startBuilding(tradePair, sevenReadyBlocks) === false){
                wrapper()
            }
            
        }else{
            setTimeout(function(){
                    wrapper()
            },10000)

        }

    }).catch(function(error){
        console.log(error)
    })
    
}else{
    wrapper()
}
}

wrapper()

var blockSeconds = 30


function startBuilding(tradePair, sevenReadyBlocks){
    
const clean = client.ws.candles(tradePair, '1m', candle => {
        
        
       // console.log(candle)
        
        
        main.push(candle.close)
        seconds.push(candle.eventTime)
        
        
        if (main.length > 1) {
            
            var last = main[main.length - 1]
            var lastSecond = seconds[seconds.length - 1]
            
            var beforeLast = main[main.length - 2]
            var beforeLastSecond = seconds[seconds.length -2]
            
            var sum = (last - beforeLast)
            var secondSum = ((lastSecond - beforeLastSecond)/1000)
            
            /**
             * summarise the blocks and seconds
             * our goal is to make 5 mins blocks
             */
            var reducer = (accumulator, currentValue) => accumulator + currentValue;
            
            sumBlockArray.push(Number(sum))
            
            var sumBlocks = sumBlockArray.reduce(reducer)
            sumSecondsArray.push(Number(secondSum.toFixed(0)))
            var sumSeconds = sumSecondsArray.reduce(reducer);
            
            
            /**
             * if 5 mins left, lest finalize the block
             */
            
            console.log(candle.symbol)
            if(sumSeconds > blockSeconds){
                //empty the block and the seconds arrays
                
                sumBlockArray = []
                sumSecondsArray = []
                
                
                if (sumBlocks < 0){
                    dbBlock = 1
                }else{
                    dbBlock = 0
                }
                
                if(blockEight.length === 0){
                    
                   blockEight = sevenReadyBlocks
                  
                }
                
                blockEight.push(dbBlock)
                
                if(blockEight.length >= 8){
                    console.log("---BlockEight "+blockEight)
                    var sumBlockEight = blockEight.reduce(reducer)
              


                    
                    if (sumBlockEight > 5){
                        
                        buyPriceAndQuantity = []
                        
                        async function getOpenOrders() {
                            
                            let priceOf = await client.book({ symbol: tradePair })
                            
                            // 0,1 ETH BuY VALUE

                            buyQuantity = (0.1 / priceOf.bids[1].price).toFixed(1)

                            buyPriceAndQuantity.push(priceOf.bids[1].price, buyQuantity)
                            
                            //console.log( buyPriceAndQuantity )
                            return buyPriceAndQuantity
                            
                        }
                        
                        getOpenOrders().then(async function (buyPriceAndQuantity) {
                            
                                var buy = await client2.order({
                                symbol: tradePair,
                                side: 'BUY',
                                type: 'LIMIT',
                                price: buyPriceAndQuantity[0],
                                timeInForce: 'GTC',
                                quantity: buyPriceAndQuantity[1],
                                recvWindow: recvWindowMilisec
                            })
                            console.log("*** BUY *** Price: " + buy.price + " Pcs: " + buy.origQty)
                            buyPriceAndQuantity.push(buy.clientOrderId)
                            return buyPriceAndQuantity
                            
                        }).then(async function (buyPriceAndQuantity) {
                            
                            sellAfterBuy(buyPriceAndQuantity)
                            blockEight = []
                            clean()
                            
                        }).catch(function(error){
                            console.log(error)
                            blockEight = []

                        })
                        
                    }else{
                        
                        blockEight = []
                        wrapper()
                        clean()

                    }
                    //remove the first element
                }
            }
            
        }
    })
    
}

var counter  = 1
async function sellAfterBuy(buyPriceAndQuantity){
    
    
    var getThisOrderStatus = await client2.getOrder({
        symbol: tradePair,
        origClientOrderId: buyPriceAndQuantity[2],
    })
    //console.log(getThisOrderStatus)
    
    if (getThisOrderStatus.status === "NEW"){
        
        
        var timeout = setTimeout(async function(){
            console.log("waiting for fulfill " + counter*2 +"0 seconds" )
            if(counter > 15){
                
                console.log(await client2.cancelOrder({
                    symbol: tradePair,
                    origClientOrderId: buyPriceAndQuantity[2],
                }))
                
                clearTimeout(timeout)
                wrapper()
                counter = 1
            }else{

                counter++
                sellAfterBuy(buyPriceAndQuantity)
            }
            
        },20000)
        
    }else if(getThisOrderStatus.status === "FILLED") {
        
        var quantity = buyPriceAndQuantity[1]
        quantity = parseFloat(quantity).toFixed(1)

            sellPrice = (buyPriceAndQuantity[0] * 1.014).toFixed(8)
            //console.log(sellPrice)
            var sell = await client2.order({
                symbol: tradePair,
                side: 'SELL',
                type: 'LIMIT',
                price: sellPrice,
                timeInForce: 'GTC',
                quantity: quantity,
                recvWindow: recvWindowMilisec
            })
            console.log("*** SELL *** FILLED price: " + sellPrice + " pcs: " + buyPriceAndQuantity[1] )
            counter = 1
            wrapper()

        }else if (getThisOrderStatus.status === "PARTIALLY_FILLED"){

            var executedQuantity = getThisOrderStatus.executedQty
            executedQuantity = parseFloat(executedQuantity).toFixed(1)

            sellPrice = (buyPriceAndQuantity[0] * 1.014).toFixed(8)
            console.log(sellPrice)
            console.log(await client2.order({
                symbol: tradePair,
                side: 'SELL',
                type: 'LIMIT',
                price: sellPrice,
                timeInForce: 'GTC',
                quantity: executedQuantity,
                recvWindow: recvWindowMilisec
            }))

            console.log(await client2.cancelOrder({
                symbol: tradePair,
                origClientOrderId: buyPriceAndQuantity[2],
            }))
            console.log("*** SELL *** PARTIALLY_FILLED price: " + sellPrice + " pcs: " + getThisOrderStatus.executedQty)
            counter = 1
            wrapper()
        }else{
            console.log("NOT SOLD: " + getThisOrderStatus.status)
            
            //wrapper()
        }


        


}


function getRandomInt(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
        }
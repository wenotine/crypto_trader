<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api', function () {
    return view('apiview');
});

Route::get('/binance','NewController@getAllEthTradePairs');
Route::get('/markets','NewController@getMarkets');
Route::get('/trades','NewController@getRecentTrades');
Route::get('/oneticker','NewController@getTicker');
Route::get('/candle','NewController@candeStick');
Route::get('/redis','NewController@redis');